﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMove : MonoBehaviour {
    [SerializeField] private float objectSpeed = 1;
    private float resetPosition = -1.192093e-07f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.back * (objectSpeed * Time.deltaTime));
        if (transform.localPosition.z <= resetPosition)
        {
            Vector3 newPos = new Vector3(120 , transform.position.y,transform.position.z);
            transform.position = newPos;
        }
	}
}
